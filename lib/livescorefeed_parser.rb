require 'httparty'
require "livescorefeed_parser/version"
require "livescorefeed_parser/soccer"

module LivescorefeedParser
  
  # User and Password from ScoresPro account
  mattr_accessor :key

  def self.setup
    yield self
  end

end
