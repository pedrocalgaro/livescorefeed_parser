module LivescorefeedParser
  class Soccer

    def self.get_matches
      options = {
        :games => "livescores",
        :key => LivescorefeedParser.key,
        :format => "json",
        :date => Date.today.to_s
      }
      games = {}
      http_response = HTTParty.get('http://livescorefeed.net/data/data.php', query: options)
      if http_response.parsed_response["maininfo"] 
        http_response.parsed_response["maininfo"].each do |id, champ|
          group_name = "#{champ['champname']} - #{champ['country']}"
          games[group_name] = []
          champ["games"].each do |game|
            game_name = "#{game['host']} x #{game['guest']}"
            games[group_name] << [game_name, game["id"]]
          end
        end
      end
      games
    end

    def self.get_scores
       options = {
        :games => "livescores",
        :key => LivescorefeedParser.key,
        :format => "json",
        :date => Date.today.to_s
      }
      games = {}
      http_response = HTTParty.get('http://livescorefeed.net/data/data.php', query: options)
      if http_response.parsed_response["maininfo"] 
        http_response.parsed_response["maininfo"].each do |id, champ|
          champ["games"].each do |game|
            games[game["id"]] = game
          end
        end
      end
      games
    end

  end
end